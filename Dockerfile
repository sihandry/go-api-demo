FROM golang:1.15
ADD . /go/src
WORKDIR /go/src/app

RUN go get -d -v ./...
RUN go install -v ./...
RUN go build -o main .

CMD ["/go/src/app/main"]