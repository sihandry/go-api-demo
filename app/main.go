package main

import (
	"fmt"
	"net/http"
)

func home(w http.ResponseWriter, req *http.Request) {
	fmt.Fprintf(w, "Home Page\n")
}

func about(w http.ResponseWriter, req *http.Request) {
	fmt.Fprintf(w, "About Page\n")
}

func main() {
	http.HandleFunc("/home", home)
	http.HandleFunc("/about", about)

	err := http.ListenAndServe(":9090", nil)
	if err != nil {
		panic(err)
	}
}
